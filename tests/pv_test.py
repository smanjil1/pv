
from mobility.pv.utils.pv_calculate import calculate_pv


def test_pv():
    sample_reading = {
        'meter_value': 6.721
    }

    pv_value, meter_pv_sum = calculate_pv(sample_reading)

    assert pv_value == 6721
    assert meter_pv_sum == 6727.721
