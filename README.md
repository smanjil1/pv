
# PV simulator

## Setup (Developed under Python3.7 virtualenv) in Ubuntu(18.04.3 LTS)
```
$ python setup.py install
$ pip install -r requirements.txt
$ sudo apt install rabbitmq-server
```

## Execution (Open two terminals)
```
$ python src/mobility/pv/broker.py  (in one terminal - this will start the
 broker and start listening and process accordingly)
$ python src/mobility/pv/meter.py  (in another terminal - this will send the
 message to the queue which broker will be listening and process)
```

### As a result, we will have a file `readings.csv` in base directory with required readings indicated for datetime stamp for 24 hours.

## Tests
`
$ pytest
`

## Run in docker
`
$ docker build -t mobility .
$ docker run --net host mobility:latest
`