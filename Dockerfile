
FROM python:3.7

ADD . /

RUN python setup.py install
RUN apt-get update -y
RUN apt-get install rabbitmq-server -y

CMD ["python", "src/mobility/pv/broker.py"]
