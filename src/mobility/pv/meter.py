
import json
import pika
import random

from datetime import datetime, timedelta

# initiating connection with pika client
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

# stating queue name
channel.queue_declare(queue='pv', durable=True)

# setting initial time to track 24 hours after the completion
current_measurement_time, initial_measurement_time = datetime.now(), \
                                                     datetime.now()

while True:
    # meter value random and continuous from 0 to 900 watts.
    meter_value = round((random.uniform(0, 9000) / 1000), 3)

    next_measurement_time = current_measurement_time + timedelta(seconds=3600)
    current_measurement_time = next_measurement_time

    # creating json of the meter reading and time to publish in channel
    meter_record = json.dumps(
        {
            'reading_time': current_measurement_time.__str__(),
            'meter_value': meter_value
        }
    )

    # publish message
    channel.basic_publish(
        exchange='',
        routing_key='pv',
        body=meter_record)
    print(f" [x] Sent {meter_record}")

    # set next tim to get new measurement
    time_difference = (next_measurement_time - initial_measurement_time)

    # check if 24 hours is completed to stop the execution
    if time_difference.days == 1:
        break

# closing the pika connection
connection.close()
