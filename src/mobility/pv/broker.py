
import csv
import json
import pika

from mobility.pv.utils.pv_calculate import calculate_pv

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost'))

channel = connection.channel()

channel.queue_declare(queue='pv', durable=True)
print(' [*] Waiting for messages. To exit press CTRL+C')


def callback(ch, method, properties, body):
    """Callback method to receive message from pika client
    Args:
        ch: channel
        method: channel method
        properties: deliver properties
        body: json message from meter
    """
    body = json.loads(body)

    # simulated PV power value, for now, I did multiply the meter reading
    # value with 1000 to change into KW.
    body['pv_value'], body['meter_pv_sum'] = calculate_pv(body)

    cw.writerow([body['reading_time'], body['meter_value'],
                 body['pv_value'], body['meter_pv_sum']])

    print(f" [x] Received {body}")
    print(" [x] Done")
    ch.basic_ack(delivery_tag=method.delivery_tag)


# file object to write the result
cw = csv.writer(open('readings.csv', 'a'))

# header for csv file
cw.writerow(['reading_time', 'meter', 'pv_value', 'meter_pv_sum'])

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='pv', on_message_callback=callback)

channel.start_consuming()
