
def calculate_pv(body):
    """PV calculation.
    Args:
        body: json holding meter values timestamp

    Returns: updated json with pv_value for meter and sum of meter and pv
    """
    pv_value = body['meter_value'] * 1000
    meter_pv_sum = body['meter_value'] + pv_value

    return round(pv_value, 3), round(meter_pv_sum, 3)
